const express = require("express")
const app = express()

app.get("/test", (req, res) => {
    res.send("Server is working correctly")
})

app.listen(8080, () => {
    console.log("API-service is working")
})

app.listen(80, () => {
    console.log(`API Service is working on port ${port}`)
    console.log(`Host is ${host}`)
})

const {port, host} = require("./configuration")
const {connectDB} = require("./db")

connectDB()
    .on('error', console.error.bind(console, 'connection error:'))
    .once("open", startServer)

function startServer() {
    app.listen(port, () => {
        console.log(`Server is running on ${host}:${port}`)
    })
}

const {User} = require("./user")

app.get('/users', async (req, res) => {
    try {
        const user = new User({firstName: "Glib", lastName: "Boreichenko"})
        await user.save()
        const users = await User.find()
        res.json({users})
    } catch (err) {
        res.send({err})
    }
})